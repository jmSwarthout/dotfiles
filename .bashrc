# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

#define some colors for later{{{:
color_Off='\e[0m'       # Text Reset

# Regular Colors
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# Bold
BBlack='\e[1;30m'       # Black
BRed='\e[1;31m'         # Red
BGreen='\e[1;32m'       # Green
BYellow='\e[1;33m'      # Yellow
BBlue='\e[1;34m'        # Blue
BPurple='\e[1;35m'      # Purple
BCyan='\e[1;36m'        # Cyan
BWhite='\e[1;37m'       # White

# Underline
UBlack='\e[4;30m'       # Black
URed='\e[4;31m'         # Red
UGreen='\e[4;32m'       # Green
UYellow='\e[4;33m'      # Yellow
UBlue='\e[4;34m'        # Blue
UPurple='\e[4;35m'      # Purple
UCyan='\e[4;36m'        # Cyan
UWhite='\e[4;37m'       # White

# Background
On_Black='\e[40m'       # Black
On_Red='\e[41m'         # Red
On_Green='\e[42m'       # Green
On_Yellow='\e[43m'      # Yellow
On_Blue='\e[44m'        # Blue
On_Purple='\e[45m'      # Purple
On_Cyan='\e[46m'        # Cyan
On_White='\e[47m'       # White

# High Intensity
IBlack='\e[0;90m'       # Black
IRed='\e[0;91m'         # Red
IGreen='\e[0;92m'       # Green
IYellow='\e[0;93m'      # Yellow
IBlue='\e[0;94m'        # Blue
IPurple='\e[0;95m'      # Purple
ICyan='\e[0;96m'        # Cyan
IWhite='\e[0;97m'       # White

# Bold High Intensity
BIBlack='\e[1;90m'      # Black
BIRed='\e[1;91m'        # Red
BIGreen='\e[1;92m'      # Green
BIYellow='\e[1;93m'     # Yellow
BIBlue='\e[1;94m'       # Blue
BIPurple='\e[1;95m'     # Purple
BICyan='\e[1;96m'       # Cyan
BIWhite='\e[1;97m'      # White

# High Intensity backgrounds
On_IBlack='\e[0;100m'   # Black
On_IRed='\e[0;101m'     # Red
On_IGreen='\e[0;102m'   # Green
On_IYellow='\e[0;103m'  # Yellow
On_IBlue='\e[0;104m'    # Blue
On_IPurple='\e[0;105m'  # Purple
On_ICyan='\e[0;106m'    # Cyan
#}}}/color def


# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend histreedit histverify

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
#enable extended globbing
shopt -s extglob
# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar
# Make sure we don't bork a bunch of files all the time with piping 
# and redirecting
set -o noclobber
# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
#if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
#    debian_chroot=$(cat /etc/debian_chroot)
#fi

# set variable identifying if we are using ssh to connect
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
	ssh=1
else
	ssh=0
fi
#check if root
if [ "$UID" == 0 ] || [ "$EUID" == 0 ]; then
	priv=priv
else
	priv=nopriv
fi

#setup git branch checking
parse_git(){
        git rev-parse --git-dir > /dev/null 2>&1;
        if [ $? -eq 0 ] ; then
                echo $branch;
        fi
}

parse_git_branch() {
        local branchName=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/*\(.*\) /\1/')
        git rev-parse --git-dir > /dev/null 2>&1;
        if [ $? -eq 0 ] ; then
                echo " $branchR$branchName$branchL "
        fi
}
# set a fancy prompt (non-color, unless we know we "want" color)
set_prompt () {
	local Last_Command=$?
	case "$TERM" in
		*color) color_prompt=yes;;
	esac
	case "$TERM" in
		xterm) color_prompt=yes;;
	esac
	#set unicode for special chars
	local section=$'\u00A7'
	local nullset=$'\u00D8'
	local doubledagger=$'\u2021'
	local branchL=$'\u00BB'
	local branchR=$'\u00AB'
	local branch=$'\u03BB'
	#start the prompt blank
	PS1=""
	PS2="\[$Blue\]$doubledagger$doubledagger$doubledagger \[$White\]"
	#add last command fail/success
	PS1+="\[$White\]$Last_Command "
	if [[ $Last_Command == 0 ]]; then
		PS1+="\[$BGreen\]$section "
	else
		PS1+="\[$BRed\]$nullset "
	fi

	if [ "$color_prompt" = yes ] && [ "$ssh" = 0 ] && [ "$priv" = nopriv ]; then
		PS1+="\\[$Blue\]\u@\h\[$White\]:\[$BBlue\][\w]"
                PS1+="\[$BGreen\] $(parse_git)\[$Green\]$(parse_git_branch)"
                PS1+="\n\[$Green\]\j $doubledagger\[$White\] "
	elif [ "$color_prompt" = yes ] && [ "$ssh" = 1 ] && [ "$priv" = nopriv ]; then
		PS1+="\\[$Green\]{ssh}\u@\h\[$White\]:\[$BBlue\] [\w]"
                PS1+="\[$BGreen\] $(parse_git)\[$Green\]$(parse_git_branch)"
                PS1+="\n\[$Green\]\j $doubledagger\[$White\] "
	elif [ "$color_prompt" = yes ] && [ "$priv" = priv ]; then
		PS1+="\\[$Red\]{root}\u@\h: \[$BRed\] [\w]\n\\[$Green\]\j \[$BRed\]#\[$White\] "
		PS2="\[$BRed\]$doubledagger$doubledagger$doubledagger \[$White\]"
	elif [ "$color_prompt" != yes ] && [ "$ssh" = 0 ]; then
	    PS1="\u@\h: [\w]\$ "
	elif [ "$color_prompt" != yes ] && [ "$ssh" = 1 ]; then
	    PS1="{ssh}\u@\h: [\w]\$ "
	fi
}
PROMPT_COMMAND='set_prompt'
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*|urxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -l'
alias la='ls -la'
alias l='ls -CF'
# aliases for better versions of things
#alias vi='vim'
alias top='htop'
alias wget='wget -c'
alias mkdir='mkdir -pv'
alias cp='cp -ir'
alias mv='mv -i'
alias rm='rm -rI'
alias bc='bc -l'
alias diff='colordiff'
alias py='python' 
alias apt-get='apt-get -q'
alias clock='tty-clock -c -b -C4 -f "%A %e %B"'
alias ping='prettyping'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

eval "$(thefuck --alias)"


# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/scripts/base16-tomorrow-night.sh"
[[ -s $BASE16_SHELL ]] && source $BASE16_SHELL
