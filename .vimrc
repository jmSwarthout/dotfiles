"enable 256 colors
set t_Co=256
"not vi mode
set nocompatible
"allow copy paste between sessions
"set paste
set clipboard^=unnamed
"syntax hightlighting
syntax on
set showmatch
set foldenable
set foldlevelstart=10
"colorscheme
let base16colorspace=256
colorscheme base16-tomorrow-night
set background=dark
"filestuff
filetype indent plugin on
set fileformat=unix
set encoding=utf-8
"indents and max line width for readability
set autoindent
set expandtab
set textwidth=79
"handle js, html, and css indent neatly
au BufNewFile,BufRead *.js,*.html,*.css,*.scss set tabstop=2 softtabstop=2 shiftwidth=2
"handle py tabs
au BufNewFile,BufRead *.py
	\set tabstop=4
	\set softtabstop=4
	\set shiftwidth=4

"nicer autocomplete command mode
set wildmenu
set wildmode=longest:list,full
"line,column in the bottom
set ruler
function! GitBranch()
        return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
        let l:branchname = GitBranch()
        return strlen(l:branchname) > 0?'  λ '.l:branchname.' ':''
endfunction
hi User1 ctermbg=18 ctermfg=20 
hi User2 ctermbg=03 ctermfg=18
hi User3 ctermbg=01 ctermfg=18
hi User4 ctermbg=05 ctermfg=18
hi User5 ctermbg=04 ctermfg=18
hi User6 ctermbg=06 ctermfg=18

set laststatus=2
set statusline=
set statusline+=%2*%{StatuslineGit()}
set statusline+=%3*\ «%f»\ 
set statusline+=%4*\ %y\ 
set statusline+=%1*\ \[%n%H%M%R%W]
set statusline+=%=
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\ \[%{&fileformat}\]\ 
"set statusline+=%4*\ «\ %3P\ ǁ 
set statusline+=%5*\ line\ %3l\ 
set statusline+=%6*\ of\ %L\  
"no more beeps
set visualbell
set t_vb=
"easier running commands w/o having to hit enter again
set cmdheight=2
"highlight for where you should linewrap
let &colorcolumn=join(range(97,999),",")
highlight ColorColumn ctermbg=18
"highlight current line
set cursorline
highlight CursorLine ctermbg=18
"linenumbers
set number
set numberwidth=5
highlight Number ctermbg=18

