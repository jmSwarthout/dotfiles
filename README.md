## Copy of dotfiles

Copy all files into `~`

For Xresources themes see
[https://github.com/chriskempson/base16-xresources](https://github.com/chriskempson/base16-xresources)
* Install Xresources themes into `~/xres`

For Vim themes see
[https://github.com/chriskempson/base16-vim](https://github.com/chriskempson/base16-vim)
* Install Vim themes into `~/.vim/colors`

For shell themes see
[https://github.com/chriskempson/base16-shell](https://github.com/chriskempson/base16-shell)
* Install shell themes into `~/.config/base16-shell`


